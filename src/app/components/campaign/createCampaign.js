import React, { Component } from 'react';

import { Link } from 'react-router';
import { connect } from 'react-redux';
import { getAudienceList, getsurveyDashboardList } from '../../actions/dashboardActions';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import Slider, { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import { getCurrentUser } from '../../actions/authActions';
const style = { width: 3000, margin: 1 };
const styleExtendedDates = { width: 2000, margin: 1 };
const numStyle = { width: 40 }

class CreateCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      audienceList: null,
      audienceKey: null,
      audienceGroupLength: null,
      selectedGroup: [],
      error: false,
      surveyDashboardList: null,
      surveySelected: null,
      surveySelectedIndex: -1,
      startDate: moment(),
      endDate: moment(),
      week: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      excludeDatesCheckbox: true,
      excludeDatesStatusArray: [true, true],
      dynamicExcludedDates: [1, 2],
      dynamicNotifyEmail: [1, 2],
      wordAlertCheckbox: true,
      dynamicNotifyEmailThreshold: [1, 2],
      wordAlertCheckboxThreshold: true,
      threshold: 0,
      surveyAcceptedCompleted: 1,
      surveyAcceptedIncomplete: 1,
      surveyRejected: 1,
      frequencyCalls: 1,
      mapFrequency: [],
      mapSurvey: [],
      mapAttempt: [],
      delayedCallBackCheckbox: true,
      dynamicAttempt: [1, 2],
      excludedDateList: [moment(new Date('10/23/2017')), moment(new Date('10/25/2017'))],
      excludedDateText: ['Thanksgiving', 'Christmas'],
      wordAlertText: '',
      notifySpecificWordEmailList: [],
      notifyThresholdWordEmailList: [],
      weekTimeList: [['8:00AM', '7:00PM'], ['8:00AM', '7:00PM'], ['8:00AM', '7:00PM'], ['8:00AM', '7:00PM'], ['8:00AM', '7:00PM'], ['8:00AM', '7:00PM'], ['8:00AM', '7:00PM']],
      weekTimeListJson: [['08:00', '19:00'], ['08:00', '19:00'], ['08:00', '19:00'], ['08:00', '19:00'], ['08:00', '19:00'], ['08:00', '19:00'], ['08:00', '19:00']],
      excludeDayTimeList: [['8:00AM', '7:00PM'], ['8:00AM', '7:00PM']],
      excludeDayTimeListJson: [['08:00', '19:00'], ['08:00', '19:00']],
      mapCallerId: ['Blocked', '222', '333'],
      mapCallbackNum: ["ANI", "Alternate Number"],
      mapCustomerIdentifier: ["Account Number", "ANI"],
      callerId: 'Blocked',
      callbackNum: 'ANI',
      customerIdentifier: 'Account Number',
      attemptList: [1,1],
      lastAttempt: 'Voicemail',
      campaignName: '',
      campaignsummary: '',
      excludeDateErr:false,
      surveyErr:true
    };

    this.props.getAudienceList().then(res => {
      //document.getElementById('aud0').appendChild = this.props.audienceList.Audience['Audience Group 1'][0]
      this.setState({
        audienceList: this.props.audienceList,
        audienceKey: Object.keys(this.props.audienceList.Audience),
        audienceGroupLength: Object.keys(this.props.audienceList.Audience).length,
        });

      console.log(this.state.audienceGroupLength);
      this.setState({selectedGroup:[this.props.audienceList.Audience['Audience Group 1'][0]] });
      let selectedGroup = this.state.selectedGroup;
      selectedGroup[0] = this.props.audienceList.Audience['Audience Group 1'][0];
      // if(e.target.value == "-1"){
      // 	selectedGroup.splice(order,1)
      // }
      this.setState({ selectedGroup: selectedGroup });
    });
    //getsurveyDashboardList
    this.props.getsurveyDashboardList().then(res => {
      this.setState({ surveyDashboardList:this.props.surveyList });
      //this.setState({ audienceGroupLength: Object.keys(res.data.Audience).length});
      console.log(this.state.surveyDashboardList);
     
      //this.setState({ campaigns: [] });
    });
  }
 componentWillMount() {
    let mapFrequency = this.state.mapFrequency;
    let mapSurvey = this.state.mapSurvey;
    let mapAttempt = this.state.mapAttempt;
    for (let i = 1; i <= 180; i++) {
      if (i <= 60) {
        mapFrequency.push(i);
        mapSurvey.push(i);
        mapAttempt.push(i);
      }
      else if (i <= 100 && i > 60) {
        mapSurvey.push(i);
        mapFrequency.push(i);
      }
      else {
        mapSurvey.push(i);
      }
    }
    this.setState({ mapFrequency: mapFrequency });
    this.setState({ mapSurvey: mapSurvey });
    this.setState({ mapAttempt: mapAttempt });
   
  }
  handleStartDateChange(date) {
    console.log(date.format('MM/DD/YYYY'));
    if (date.diff(moment(), 'days') < 0) {
      this.setState({ startDate: moment() });
    } else {
      this.setState({ startDate: date });
    }
  }
  handleEndDateChange(date) {
    console.log(date.format('MM/DD/YYYY'));
    if (date.diff(this.state.startDate, 'days') < 0) {
      this.setState({ endDate: this.state.startDate });
    } else {
      this.setState({ endDate: date});
    }
  }
  _handleSurveySelect(e) {
    let surveyDashboardList = this.state.surveyDashboardList.SurveyDashboard[Number(e.target.value)];
    console.log(surveyDashboardList)
    this.setState({ surveySelected: surveyDashboardList });
    console.log(this.state.surveySelected);
    this.setState({ surveySelectedIndex: Number(e.target.value) })
    if(surveyDashboardList){
      this.setState({surveyErr:false})
    }
    else{
      this.setState({surveyErr:true})
    }
   
  }
  _handleChangeSelect(order, e) {
    let selectedGroup = this.state.selectedGroup;
    selectedGroup[order] = e.target.value;
    // if(e.target.value == "-1"){
    // 	selectedGroup.splice(order,1)
    // }
    this.setState({ selectedGroup: selectedGroup });
    let flag = false;
    for (let i = 0; i < selectedGroup.length; i++) {
      if (selectedGroup[i] != '0') {
        this.setState({ error: false });
        flag = true;
        break;
      }
    }
    if (flag == false) {
      this.setState({ error: true });
    }
   
    console.log(this.state.error);
    console.log(this.state.selectedGroup);
  }
  _addAudience() {
    this.state.audienceKey.push({});
    this.setState({ audienceKey: this.state.audienceKey });
    console.log(this.state.audienceKey);
  }

  _setWeek(val) {
    let week = this.state.week;
    if (week.indexOf(val) >= 0) {
      week.splice(week.indexOf(val), 1);
    }
    else {
      week.push(val)
    }
    this.setState({ week: week })
  }
  _setExcludeDateCheckbox() {
    debugger;
    let status = this.state.excludeDatesCheckbox;
    this.setState({ excludeDatesCheckbox: !status });
    console.log(this.state.excludeDatesCheckbox)
    let excludeDatesStatusArray = this.state.excludeDatesStatusArray;
    for (let i = 0; i < excludeDatesStatusArray.length; i++) {
      excludeDatesStatusArray[i] = false
    }
    this.setState({ excludeDatesStatusArray: excludeDatesStatusArray })
    this._validateExcludedDates();
  }
  _setExcludeDatesStatusArray(val) {
    debugger;
    if (this.state.excludeDatesCheckbox == false) {
      return true
    }
    let excludeDatesStatusArray = this.state.excludeDatesStatusArray;
    excludeDatesStatusArray[val] = !excludeDatesStatusArray[val];

    this.setState({ excludeDatesStatusArray: excludeDatesStatusArray });
    console.log(this.state.excludeDatesStatusArray[val]);
    this._validateExcludedDates();
  }
  _createNewExcludeDay() {
    let excludeDatesStatusArray = this.state.excludeDatesStatusArray;
    excludeDatesStatusArray[excludeDatesStatusArray.length] = true;
    this.setState({ excludeDatesStatusArray: excludeDatesStatusArray })


    let orig = this.state.dynamicExcludedDates
    orig.push(this.state.dynamicExcludedDates.length + 1);
    this.setState({ dynamicExcludedDates: orig })
    console.log(this.state.dynamicExcludedDates)

    let excludedDateList = this.state.excludedDateList;
    excludedDateList.push(moment());
    this.setState({ excludedDateList: excludedDateList });
    let excludedDateText = this.state.excludedDateText;
    excludedDateText.push('');
    this.setState({ excludedDateText: excludedDateText });
    let excludeDayTimeList = this.state.excludeDayTimeList;
    let excludeDayTimeListJson = this.state.excludeDayTimeListJson;
    excludeDayTimeList[excludeDayTimeList.length] = ['8:00AM', '7:00PM'];
    excludeDayTimeListJson[excludeDayTimeListJson.length] = ['08:00', '07:00'];
    this.setState({ excludeDayTimeListJson: excludeDayTimeListJson });
    this.setState({ excludeDayTimeList: excludeDayTimeList });
    this._validateExcludedDates();
    
  }
  _validateSurveySelect(){
    if(this.state.surveySelected){
      this.setState({surveyErr:false})
      return false;
    }
    else{
      this.setState({surveyErr:true})
      return true;
    }
  }
  _validateExcludedDates(){
    debugger
    let excludedDateList = this.state.excludedDateList;
    let excludedDateText = this.state.excludedDateText;
    this.setState({excludeDateErr:false});
    for(let i = 0;i<excludedDateList.length;i++){
      if(excludedDateText[i].trim()=='' && this.state.excludeDatesStatusArray[i] == true){
        this.setState({excludeDateErr:true});
        return true;
        break;
      }
    }
    return false;
  }
  _logSlider(index, value) {
    let weekTimeList = this.state.weekTimeList;
    let weekTimeListJson = this.state.weekTimeListJson;
    let startTime = (value[0] / 2);
    let endTime = (value[1] / 2);
    let endTimeJson;
    let startTimeJson;
    if (startTime.toString().length >= 3) {
      let hours = Math.floor(startTime);
      let minutes = 30;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      startTimeJson = String(Math.floor(startTime)).length == 1 ? ('0' + String(Math.floor(startTime)) + ':' + minutes) : (String(Math.floor(startTime)) + ':' + minutes)

      startTime = hours + ':' + minutes + ampm;

      console.log(startTimeJson)
      console.log(startTime)
    }
    else {
      let hours = Math.floor(startTime);
      let minutes = 0;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      startTimeJson = String(Math.floor(startTime)).length == 1 ? ('0' + String(Math.floor(startTime)) + ':' + minutes) : (String(Math.floor(startTime)) + ':' + minutes)

      startTime = hours + ':' + minutes + ampm;

      console.log(startTimeJson)
      console.log(startTime)
    }
    if (endTime.toString().length >= 3) {
      let hours = Math.floor(endTime)
      let minutes = 30;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      endTimeJson = String(Math.floor(endTime)).length == 1 ? ('0' + String(Math.floor(endTime)) + ':' + minutes) : (String(Math.floor(endTime)) + ':' + minutes)

      endTime = hours + ':' + minutes + ampm;
      console.log(endTimeJson)
      console.log(endTime)
    }
    else {
      let hours = Math.floor(endTime);
      let minutes = 0;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      endTimeJson = String(Math.floor(endTime)).length == 1 ? ('0' + String(Math.floor(endTime)) + ':' + minutes) : (String(Math.floor(endTime)) + ':' + minutes)

      endTime = hours + ':' + minutes + ampm;
      console.log(endTimeJson)
      console.log(endTime)

    }
    weekTimeList[index] = [startTime, endTime]
    weekTimeListJson[index] = [startTimeJson, endTimeJson]
    console.log(value);
    //console.log(day); 
    this.setState({ weekTimeListJson: weekTimeListJson });
    this.setState({ weekTimeList: weekTimeList });
    console.log(this.state.weekTimeList);

  }
  _logSliderExcluded(index, value) {
    let excludeDayTimeList = this.state.excludeDayTimeList;
    let excludeDayTimeListJson = this.state.excludeDayTimeListJson;
    let startTime = (value[0] / 2);
    let endTime = (value[1] / 2);
    let endTimeJson;
    let startTimeJson;
    if (startTime.toString().length >= 3) {
      let hours = Math.floor(startTime);
      let minutes = 30;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      startTimeJson = String(Math.floor(startTime)).length == 1 ? ('0' + String(Math.floor(startTime)) + ':' + minutes) : (String(Math.floor(startTime)) + ':' + minutes)

      startTime = hours + ':' + minutes + ampm;
      console.log(startTime)
    }
    else {
      let hours = Math.floor(startTime)
      let minutes = 0;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      startTimeJson = String(Math.floor(startTime)).length == 1 ? ('0' + String(Math.floor(startTime)) + ':' + minutes) : (String(Math.floor(startTime)) + ':' + minutes)

      startTime = hours + ':' + minutes + ampm;
      console.log(startTime)
    }
    if (endTime.toString().length >= 3) {
      let hours = Math.floor(endTime)
      let minutes = 30;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      endTimeJson = String(Math.floor(endTime)).length == 1 ? ('0' + String(Math.floor(endTime)) + ':' + minutes) : (String(Math.floor(endTime)) + ':' + minutes)

      endTime = hours + ':' + minutes + ampm;
      console.log(endTime)
    }
    else {
      let hours = Math.floor(endTime)
      let minutes = 0;
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      endTimeJson = String(Math.floor(endTime)).length == 1 ? ('0' + String(Math.floor(endTime)) + ':' + minutes) : (String(Math.floor(endTime)) + ':' + minutes)

      endTime = hours + ':' + minutes + ampm;
      console.log(endTime)

    }
    excludeDayTimeList[index] = [startTime, endTime]
    excludeDayTimeListJson[index] = [startTimeJson, endTimeJson]
    console.log(value);
    //console.log(day); 
    this.setState({ excludeDayTimeListJson: excludeDayTimeListJson });
    this.setState({ excludeDayTimeList: excludeDayTimeList });
    console.log(this.state.excludeDayTimeList);

  }
  _setWordAlert() {
    let status = this.state.wordAlertCheckbox;
    this.setState({ wordAlertCheckbox: !status })
  }
  _createNotifyEmail() {
    let dynamicNotifyEmail = this.state.dynamicNotifyEmail;
    dynamicNotifyEmail.push(dynamicNotifyEmail.length + 1);
    this.setState({ dynamicNotifyEmail: dynamicNotifyEmail });
  }
  _createNotifyEmailThreshold() {
    let dynamicNotifyEmailThreshold = this.state.dynamicNotifyEmailThreshold;
    dynamicNotifyEmailThreshold.push(dynamicNotifyEmailThreshold.length + 1);
    this.setState({ dynamicNotifyEmailThreshold: dynamicNotifyEmailThreshold });
  }
  _setWordAlertThreshold() {
    let status = this.state.wordAlertCheckboxThreshold;
    this.setState({ wordAlertCheckboxThreshold: !status })
  }
  _removeNotifyEmail(index) {
    let dynamicNotifyEmail = this.state.dynamicNotifyEmail;
    dynamicNotifyEmail.splice(index, 1);
    this.setState({ dynamicNotifyEmail: dynamicNotifyEmail });
    let notifySpecificWordEmailList = this.state.notifySpecificWordEmailList;
    notifySpecificWordEmailList.splice(index, 1);
    this.setState({ notifySpecificWordEmailList: notifySpecificWordEmailList });
  }
  _removeNotifyEmailThreshold(index) {
    let dynamicNotifyEmailThreshold = this.state.dynamicNotifyEmailThreshold;
    dynamicNotifyEmailThreshold.splice(index, 1);
    this.setState({ dynamicNotifyEmailThreshold: dynamicNotifyEmailThreshold });
    let notifyThresholdWordEmailList = this.state.notifyThresholdWordEmailList;
    notifyThresholdWordEmailList.splice(index, 1);
    this.setState({ notifyThresholdWordEmailList: notifyThresholdWordEmailList });
  }
  _setThresholdNumber(e) {
    if (e.target.value < 0 || e.target.value > 10) {
      return true;
    }
    this.setState({ threshold: e.target.value })
  }

  _setSurveyAcceptedCompleted(e) {
    if (e.target.value < 1 || e.target.value > 180) {
      return true;
    }
    this.setState({ surveyAcceptedCompleted: e.target.value })
  }
  _setSurveyAcceptedIncompleted(e) {
    if (e.target.value < 1 || e.target.value > 180) {
      return true;
    }
    this.setState({ surveyAcceptedIncomplete: e.target.value })
  }
  _setSurveyRejected(e) {
    if (e.target.value < 1 || e.target.value > 180) {
      return true;
    }
    this.setState({ surveyRejected: e.target.value })
  }
  _setFrequencyCalls(e) {
    if (e.target.value < 1 || e.target.value > 100) {
      return true;
    }
    this.setState({ frequencyCalls: e.target.value })
    console.log(this.state.frequencyCalls);
  }
  _setDelayedCallBackCheckbox() {
    let delayedCallBackCheckbox = !this.state.delayedCallBackCheckbox
    this.setState({ delayedCallBackCheckbox: delayedCallBackCheckbox });
  }
  _setDynamicAttempt() {
    let dynamicAttempt = this.state.dynamicAttempt;
    dynamicAttempt.push(dynamicAttempt.length + 1);
    this.setState({ dynamicAttempt: dynamicAttempt })

  }
  _removeDynamicAttempt(index) {
    let dynamicAttempt = this.state.dynamicAttempt;
    dynamicAttempt.splice(index, 1);
    this.setState({ dynamicAttempt: dynamicAttempt });
    let attemptList = this.state.attemptList;
    attemptList.splice(index, 1)
    this.setState({ attemptList: attemptList });
  }
  _handleExcludedDateChange(index, date) {
    console.log(date.format('MM/DD/YYYY'))
    console.log(index)
    let excludedDateList = this.state.excludedDateList;
    excludedDateList[index] = date;
    this.setState({ excludedDateList: excludedDateList })
    this._validateExcludedDates();
  }
  _setExcludedDateText(index, e) {
    let excludedDateText = this.state.excludedDateText;
    excludedDateText[index] = e.target.value;
    this.setState({ excludedDateText: excludedDateText })
    this._validateExcludedDates();
  }
  _setWordAlertText(e) {
    //let wordAlertText = this.state.wordAlertText;
    let wordAlertText = e.target.value;
    this.setState({ wordAlertText: wordAlertText });
    console.log(this.state.wordAlertText)
  }
  _setNotifySpecificWordEmailList(index, e) {
    let notifySpecificWordEmailList = this.state.notifySpecificWordEmailList;
    notifySpecificWordEmailList[index] = e.target.value;
    this.setState({ notifySpecificWordEmailList: notifySpecificWordEmailList });
  }
  _setNotifyThresholdWordEmailList(index, e) {
    let notifyThresholdWordEmailList = this.state.notifyThresholdWordEmailList;
    notifyThresholdWordEmailList[index] = e.target.value;
    this.setState({ notifyThresholdWordEmailList: notifyThresholdWordEmailList });
  }
  _setCallerId(e) {
    this.setState({ callerId: e.target.value });
    console.log(this.state.callerId);
  }
  _setCallbackNum(e) {
    this.setState({ callbackNum: e.target.value });
    console.log(this.state.callbackNum);
  }
  _setCustomerIdentifier(e) {
    this.setState({ customerIdentifier: e.target.value });
    console.log(this.state.customerIdentifier);
  }
  _setAttemptList(index, e) {
    let attemptList = this.state.attemptList;
    attemptList[index] = e.target.value;
    this.setState({ attemptList: attemptList });
  }
  _setLastAttempt(e) {
    let lastAttempt = e.target.value;
    this.setState({ lastAttempt: lastAttempt });
  }
  _setCampaignName(e) {
    this.setState({ campaignName: e.target.value })
  }
  _setCampaignSummary(e) {
    this.setState({ campaignsummary: e.target.value })
  }
  _setJsonStruc() {
    let hasError = this._validateSurveySelect();
    if(hasError){
      return true;
    }
    let excludeObj={};
    debugger
    for (let i = 0; i < this.state.excludeDatesStatusArray.length; i++) {
      if (this.state.excludeDatesStatusArray[i] && this.state.excludeDatesStatusArray[i] != '') {
        excludeObj[this.state.excludedDateText[i]] = [this.state.excludedDateList[i].format('MM/DD/YYYY')];
      }
    }
    console.log(excludeObj)
    let weekTimeListJson = this.state.weekTimeListJson;
    let obj = {
      Name: this.state.campaignName,
      Summary: this.state.campaignsummary,
      author: [this.props.user, 'email'],
      Created: moment().format('MM/DD/YYYY'),
      Modified: '',
      StartDate: this.state.startDate.format('MM/DD/YYYY'),
      EndDate: this.state.endDate.format('MM/DD/YYYY'),
      Description: "This is meta-data stuff",
      Survey: {
        Name: this.state.surveySelected.Name,
        ID: this.state.surveySelected.SurveyID
      },
      Audience: this.state.selectedGroup,
      Schedule: {
        Sunday: {
          Enabled: this.state.week.indexOf('Su') >= 0 ? true : false,
          Start: weekTimeListJson[0][0],
          End: weekTimeListJson[0][1]
        },
        Monday: {
          Enabled: this.state.week.indexOf('Mo') >= 0 ? true : false,
          Start: weekTimeListJson[1][0],
          End: weekTimeListJson[1][1]
        },
        Tuesday: {
          Enabled: this.state.week.indexOf('Tu') >= 0 ? true : false,
          Start: weekTimeListJson[2][0],
          End: weekTimeListJson[2][1]
        },
        Wednesday: {
          Enabled: this.state.week.indexOf('We') >= 0 ? true : false,
          Start: weekTimeListJson[3][0],
          End: weekTimeListJson[3][1]
        },
        Thursday: {
          Enabled: this.state.week.indexOf('Th') >= 0 ? true : false,
          Start: weekTimeListJson[4][0],
          End: weekTimeListJson[4][1]
        },
        Friday: {
          Enabled: this.state.week.indexOf('Fr') >= 0 ? true : false,
          Start: weekTimeListJson[5][0],
          End: weekTimeListJson[5][1]
        },
        Saturday: {
          Enabled: this.state.week.indexOf('Sa') >= 0 ? true : false,
          Start: weekTimeListJson[6][0],
          End: weekTimeListJson[6][1]
        }
      },
      Exclude: excludeObj,
      Settings: {
        CallerID: this.state.callerId,
        ContactNumber: this.state.callbackNum,
        OfferFrequency: this.state.frequencyCalls,
        CustomerIdentifier: this.state.customerIdentifier
      },
      DelayedCallback: {
        Enabled: this.state.delayedCallBackCheckbox,
        Attempts: this.state.attemptList,
        Actions: this.state.lastAttempt

      }
    }
    console.log(obj);
  }
  render() {
    console.log("----")
    console.log(this.props.user)
     return (
      <section className="left-section">
        <div className="navicon">
          <i className="fa fa-bars" />
        </div>
        <div className="wrapper campaigns">
          <form>
            <h4>New Campaign</h4>
            <div className="cont new-campaign">
              <div className="form-group campaign-name">
                <label>CAMPAIGN NAME</label>
                <textarea type="text" className="form-control" placeholder="Enter Campaign Name" value={this.state.campaignName} onChange={this._setCampaignName.bind(this)} />
                <small className="form-text text-danger text-hide">Text validation</small>
              </div>

              <div className="form-group">
                <label>CAMPAIGN SUMMARY</label>
                <textarea type="text" className="form-control" placeholder="Enter Summary" value={this.state.campaignsummary} onChange={this._setCampaignSummary.bind(this)} />
                <small className="form-text text-danger text-hide">Text validation</small>
              </div>
            </div>

            <h4>Audience</h4>
            <div className="cont audience">
              {this.state.error ? <p>Please Select atleast one of the Option</p> : ''}
        
              {this.state.audienceList &&
                this.state.audienceKey.map((keys, index) => (
                  <div className={'custom-select-option mb-4'} key={index}>
                    <select id = {"aud"+index} value={this.state.selectedGroup[index]?this.state.selectedGroup[index]:''} className="form-control" onChange={this._handleChangeSelect.bind(this, index)}>
                      <option key={"-1"} value="0" defaultValue>
                        Select audience
                      </option>
                      {this.state.audienceList.Audience[keys] &&
                        this.state.audienceList.Audience[keys].map(function (val, index) {
                          return (
                            <option key={index} value={val} defaultValue={keys == 'Audience Group 1' && index == 0 ? true : false}>
                              {val}
                            </option>
                          );
                        })}
                    </select>
                  </div>
                ))}

              <br />
              <button className="btn bg-lightblue" type="button" onClick={this._addAudience.bind(this)}>
                <i className="fa fa-plus-circle" />
                <span>Add Audience</span>
              </button>
            </div>

            <h4>Survey</h4>
            <div className="cont audience">
            {this.state.surveyErr?<p>Please Select a Survey</p>:''}
              <div className="custom-select-option">
                <select className="form-control" value={this.state.surveySelectedIndex!="-1"?this.state.surveyDashboardList[this.state.surveySelectedIndex]:'Select survey'} onChange={this._handleSurveySelect.bind(this)}>
                  <option key={"-1"} value="-1">
                    Select survey
                  </option>
                  {this.state.surveyDashboardList &&
                    this.state.surveyDashboardList.SurveyDashboard.map(function (val, index) {
                      return (
                        <option key={index+1} value={index}>
                          {val.Name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <br />
              <Link to="surveys">
                <button className="btn bg-lightblue" type="button">
                  <i className="fa fa-plus-circle" />
                  <span>Creat New Survey</span>
                </button>
              </Link>
            </div>

            <h4>Schedule</h4>
            <div className="cont schedule">
              <div className="form-group mb-4 pb-3">
                <label>
                  <span className="fw-500">CAMPAIGN START AND END DATE</span>
                </label>
                <div>
                  <div />
                  <div className="row">
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <DatePicker
                              className="form-control text-center start-date d-inline mr-4"
                              selected={this.state.startDate}
                              onChange={this.handleStartDateChange.bind(this)}
                            />
                          </td>
                          <td>
                            <DatePicker
                              className="form-control text-center end-date d-inline"
                              selected={this.state.endDate}
                              onChange={this.handleEndDateChange.bind(this)}
                            />
                          </td>
                        </tr>
                      </tbody>
                    </table>



                  </div>
                </div>
              </div>
              <div className="schedule-elements">
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('Su') < 0 ? "day sr-1 mr-4" : "day active sr-1 mr-4"} onClick={this._setWeek.bind(this, 'Su')}>S</div>
                  <div className="fw-500 start-time-1" />
                  <div className="fw-500 end-time-1" />
                  {this.state.weekTimeList[0][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 0)} disabled={this.state.week.indexOf('Su') < 0 ? true : false} />
                  </div>
                  <span>{this.state.weekTimeList[0][1]}</span>
                </div>
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('Mo') < 0 ? "day sr-2 mr-4" : "day active sr-2 mr-4"} onClick={this._setWeek.bind(this, 'Mo')}>M</div>
                  <div className="fw-500 start-time-2" />
                  <div className="fw-500 end-time-2" />
                  {this.state.weekTimeList[1][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 1)} disabled={this.state.week.indexOf('Mo') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[1][1]}
                </div>
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('Tu') < 0 ? "day sr-3 mr-4" : "day active sr-3 mr-4"} onClick={this._setWeek.bind(this, 'Tu')}>T</div>
                  <div className="fw-500 start-time-3" />
                  <div className="fw-500 end-time-3" />
                  {this.state.weekTimeList[2][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 2)} disabled={this.state.week.indexOf('Tu') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[2][1]}
                </div>
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('We') < 0 ? "day sr-4 mr-4" : "day active sr-4 mr-4"} onClick={this._setWeek.bind(this, 'We')}>W</div>
                  <div className="fw-500 start-time-4" />
                  <div className="fw-500 end-time-4" />
                  {this.state.weekTimeList[3][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 3)} disabled={this.state.week.indexOf('We') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[3][1]}
                </div>
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('Th') < 0 ? "day sr-5 mr-4" : "day active sr-5 mr-4"} onClick={this._setWeek.bind(this, 'Th')}>T</div>
                  <div className="fw-500 start-time-5" />
                  <div className="fw-500 end-time-5" />
                  {this.state.weekTimeList[4][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 4)} disabled={this.state.week.indexOf('Th') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[4][1]}
                </div>
                <div className="d-flex mb-2">
                  <div className={this.state.week.indexOf('Fr') < 0 ? "day sr-6 mr-4" : "day active sr-6 mr-4"} onClick={this._setWeek.bind(this, 'Fr')}>F</div>
                  <div className="fw-500 start-time-6" />
                  <div className="fw-500 end-time-6" />
                  {this.state.weekTimeList[5][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 5)} disabled={this.state.week.indexOf('Fr') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[5][1]}
                </div>
                <div className="d-flex">
                  <div className={this.state.week.indexOf('Sa') < 0 ? "day sr-7 mr-4" : "day active sr-7 mr-4"} onClick={this._setWeek.bind(this, 'Sa')}>S</div>
                  <div className="fw-500 start-time-7" />
                  <div className="fw-500 end-time-7" />
                  {this.state.weekTimeList[6][0]}
                  <div style={style}>
                    <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSlider.bind(this, 6)} disabled={this.state.week.indexOf('Sa') < 0 ? true : false} />
                  </div>
                  {this.state.weekTimeList[6][1]}
                </div>

                <div className="custom-checkbox exclude-dates">

                  <input type="checkbox" onClick={this._setExcludeDateCheckbox.bind(this)} checked={this.state.excludeDatesCheckbox ? true : false} />

                  <label htmlFor="checkbox01" onClick={this._setExcludeDateCheckbox.bind(this)}>
                    <span className="text-lightblue">Exclude Dates</span>
                  </label>
                </div>
                {this.state.excludeDateErr?<span className="dashboard text-error">Please fill the empty fields</span>:''}
                {
                  this.state.dynamicExcludedDates.map(function (val, index) {
                    console.log(this)
                    return (<div>
                      <div className="ml-5 pl-1">

                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <input disabled={!this.state.excludeDatesCheckbox || !this.state.excludeDatesStatusArray[index]} type="text" className="form-control form-lightblue d-inline fw-500 thanksgiving w-100 mr-2" value={this.state.excludedDateText[index]} onChange={this._setExcludedDateText.bind(this, index)} />
                              </td>
                              <td>
                                <DatePicker
                                  disabled={!this.state.excludeDatesCheckbox || !this.state.excludeDatesStatusArray[index]}
                                  className="form-control form-lightblue d-inline thanksgiving-date text-center fw-500 w-100"
                                  selected={this.state.excludedDateList[index]}
                                  onChange={this._handleExcludedDateChange.bind(this, index)}
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>




                        {/* <input type="text" value="10/23/17" className="form-control form-lightblue d-inline thanksgiving-date text-center fw-500 w-100" />
                    */}</div>
                      <div className="d-flex ml-3 mt-3 mb-2 pl-1">
                        <div className="custom-checkbox cs-1" onClick={this._setExcludeDatesStatusArray.bind(this, index)}>
                          <input type="checkbox" checked={this.state.excludeDatesStatusArray[index] ? true : false} />
                          <label htmlFor="cs-1" >&nbsp;</label>

                        </div>
                        {this.state.excludeDayTimeList[index][0]}
                        <div style={styleExtendedDates}>
                          <Range min={0} max={48} step={1} defaultValue={[16, 38]} onChange={this._logSliderExcluded.bind(this, index)} disabled={this.state.excludeDatesCheckbox && this.state.excludeDatesStatusArray[index] ? false : true} />
                        </div>
                        {this.state.excludeDayTimeList[index][1]}
                        <div className="fw-500 start-time-8" />
                        <div className="slider-range slider-range-8 mx-3 mt-2" />
                        <div className="fw-500 end-time-8" />
                      </div>
                    </div>
                    )



                  }, this)}

                <div className="ml-3">
                  &nbsp;<button disabled={!this.state.excludeDatesCheckbox} className="btn bg-lightblue btn-add" type="button" onClick={this._createNewExcludeDay.bind(this)}>
                    <i className="fa fa-plus" />
                  </button>
                </div>
                {/*                 
                <div className="ml-5 pl-1">
                  <input type="text" className="form-control form-lightblue d-inline fw-500 thanksgiving w-100 mr-2" value="Thanksgiving" disabled={this.state.excludeDatesCheckbox?false:true} />
                  <input type="text" value="10/23/17" className="form-control form-lightblue d-inline thanksgiving-date text-center fw-500 w-100" disabled={this.state.excludeDatesCheckbox?false:true}/>
                </div>
                <div className="d-flex ml-3 mt-3 mb-2 pl-1">
                  <div className="custom-checkbox cs-1" onClick={this._setExcludeDatesStatusArray.bind(this,0)}>
                    <input type="checkbox"   checked = {this.state.excludeDatesStatusArray[0] ?true:false} disabled={this.state.excludeDatesCheckbox?false:true} />
                    <label htmlFor="cs-1" >&nbsp;</label>
                  </div>
                  <div className="fw-500 start-time-8" />
                  <div className="slider-range slider-range-8 mx-3 mt-2" />
                  <div className="fw-500 end-time-8" />
                </div>
                <div className="ml-5 pl-1">
                  <input type="text" className="form-control form-lightblue d-inline fw-500 thanksgiving w-100 mr-2" value="Christmas" disabled={this.state.excludeDatesCheckbox?false:true} />
                  <input type="text" value="10/25/17" className="form-control form-lightblue d-inline thanksgiving-date text-center fw-500 w-100" disabled={this.state.excludeDatesCheckbox?false:true} />
                </div>
                <div className="d-flex ml-3 mt-3 mb-4 pl-1">
                  <div className="custom-checkbox cs-2" onClick={this._setExcludeDatesStatusArray.bind(this,1)}>
                    <input type="checkbox" checked = {this.state.excludeDatesStatusArray[1] ?true:false} disabled={this.state.excludeDatesCheckbox?false:true}/>
                    <label >&nbsp;</label>
                  </div>
                  <div className="fw-500 start-time-9" />
                  <div className="slider-range slider-range-9 mx-3 mt-2" />
                  <div className="fw-500 end-time-9" />
                </div>
								{this.state.dynamicExcludedDates}
                <div className="ml-3">
                  &nbsp;<button className="btn bg-lightblue btn-add" type="button" onClick = {this._createNewExcludeDay.bind(this,this.state.excludeDatesStatusArray.length)}>
                    <i className="fa fa-plus" />
                  </button>
                </div> */}
              </div>
            </div>

            <h4>Configuration</h4>
            <div className="cont configuration">
              <div className="row mb-4">
                <div className="col-6">
                  <div className="text-lightblue">Caller ID</div>
                </div>
                <div className="col-6 pl-1">
                  <div className="custom-select-option primary" >
                    <select className="form-control" onChange={this._setCallerId.bind(this)}>
                      {this.state.mapCallerId.map(function (val, index) {
                        if (index == 0) {
                          return <option defaultValue key={index+1} value={val}>{val}</option>
                        }
                        return <option key={index+1} value={val}>{val}</option>
                      })}
                      {/* <option selected>Blocked</option>
                      <option>Lorem</option>
                      <option>Ipsum</option>
                      <option>Dolor</option> */}
                    </select>
                  </div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col-6">
                  <div className="text-lightblue">Call back number</div>
                </div>
                <div className="col-6 pl-1">
                  <div className="custom-select-option primary">
                    <select className="form-control" onChange={this._setCallbackNum.bind(this)}>
                      {this.state.mapCallbackNum.map(function (val, index) {
                        if (index == 0) {
                          return <option defaultValue key={index+1} value={val}>{val}</option>
                        }
                        return <option key={index+1} value={val}>{val}</option>
                      })}
                      {/* <option selected>Home_Number</option>
                      <option>Lorem</option>
                      <option>Ipsum</option>
                      <option>Dolor</option> */}
                    </select>
                  </div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col-6">
                  <div className="text-lightblue">Customer Identifier</div>
                </div>
                <div className="col-6 pl-1">
                  <div className="custom-select-option primary" >
                    <select className="form-control" onChange={this._setCustomerIdentifier.bind(this)}>
                      {this.state.mapCustomerIdentifier.map(function (val, index) {
                        if (index == 0) {
                          return <option defaultValue key={index+1} value={val}>{val}</option>
                        }
                        return <option key={index+1} value={val}>{val}</option>
                      })}
                      {/* <option selected>Account Number</option>
                      <option>Lorem</option>
                      <option>Ipsum</option>
                      <option>Dolor</option> */}
                    </select>
                  </div>
                </div>
              </div>

              <div className="row mb-2">
                <div className="col-5 col-sm-6">
                  <div className="text-lightblue">Frequency Per Customer</div>
                </div>
                <div className="col-7 col-sm-6 pl-1">
                  <span className="text-lightblue pl-5">Every</span>
                  <span className="custom-select-option w-50px primary every-call mx-1">
                    <select className="form-control" onChange={this._setFrequencyCalls.bind(this)}>

                      {
                        this.state.mapFrequency.map(function (val, index) {
                          if (index == 0) {
                            return <option key={index+1} value={val} defaultValue>{val}</option>
                          }
                          return <option key={index+1} value={val}>{val}</option>

                        }, this)
                      }

                    </select>
                  </span>
                  <span className="text-lightblue">call</span>
                </div>
              </div>

              <div className="float-right mb-1">
                <span className="text-lightblue">Survey Accepted &amp; Completed: Days</span>
                <span className="custom-select-option w-50px primary mx-1">
                  <select className="form-control" onChange={this._setSurveyAcceptedCompleted.bind(this)}>
                    {
                      this.state.mapSurvey.map(function (val, index) {
                        if (index == 0) {
                          return <option key={index+1} value={val} defaultValue>{val}</option>
                        }
                        return <option key={index+1} value={val}>{val}</option>

                      }, this)
                    }
                  </select>
                </span>
              </div>

              <div className="float-right mb-1">
                <span className="text-lightblue">Survey Accepted &amp; Completed: Days</span>
                <span className="custom-select-option w-50px primary mx-1">
                  <select className="form-control" onChange={this._setSurveyAcceptedIncompleted.bind(this)}>
                    {
                      this.state.mapSurvey.map(function (val, index) {
                        if (index == 0) {
                          return <option key={index} value={val} defaultValue>{val}</option>
                        }
                        return <option key={index} value={val}>{val}</option>

                      }, this)
                    }
                  </select>
                </span>
              </div>
              <div className="clearfix" />

              <div className="float-right mb-2">
                <span className="text-lightblue pr-4 mr-5">Survey Rejected : Days</span>&nbsp;&nbsp;
                <span className="custom-select-option w-50px primary mx-1">
                  <select className="form-control" onChange={this._setSurveyRejected.bind(this)}>
                    {
                      this.state.mapSurvey.map(function (val, index) {
                        if (index == 0) {
                          return <option key={index} value={val} defaultValue>{val}</option>
                        }
                        return <option key={index} value={val}>{val}</option>

                      }, this)
                    }
                  </select>
                </span>
              </div>
              <div className="clearfix" />

              <div className="row">
                <div className="col-5 pr-0">
                  <div className="custom-checkbox">
                    <input type="checkbox" checked={this.state.delayedCallBackCheckbox} />
                    <label htmlFor="checkbox02" onClick={this._setDelayedCallBackCheckbox.bind(this)}>
                      <span className="text-lightblue">Delayed Callback</span>
                    </label>
                    <span className="help-circled ml-1">
                      <i className="fa fa-question-circle" />
                    </span>
                  </div>
                </div>
                <div className="col-7 py-1">
                  {
                    this.state.dynamicAttempt.map(function (val, index) {
                      return (
                        <div className="mb-1">
                          <span className="fw-500 ml-1">Attempt {index + 1}:&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;
                      <span className="custom-select-option w-50px primary mx-1">
                            <select disabled={!this.state.delayedCallBackCheckbox} className="form-control" onChange={this._setAttemptList.bind(this, index)}>
                              {this.state.mapAttempt.map(function (val, ind) {
                                if (ind == 0) {
                                  return <option key={ind} value={val} defaultValue>{val}</option>
                                }
                                return <option key={ind} value={val}>{val}</option>

                              }, this)}
                            </select>
                          </span>
                          <span className="fw-500">min</span>&nbsp;&nbsp;&nbsp;
                      {this.state.delayedCallBackCheckbox ?
                            <i className="fa fa-trash-o" onClick={this._removeDynamicAttempt.bind(this, index)} aria-hidden="true"></i>
                            : <i className="fa fa-trash-o" aria-hidden="true"></i>

                          }
                        </div>
                      )
                    }, this)
                  }
                  {/* <div className="mb-1">
                    <span className="fw-500 ml-1">1st Attempt</span> <input type="number" className="form-control form-lightblue mx-1 d-inline" value="05" />{' '}
                    <span className="fw-500">min</span>
                  </div>
                  <div className="mb-1">
                    <span className="fw-500 ml-1">2nd Attempt</span> <input type="number" className="form-control form-lightblue mr-1 d-inline" value="15" />{' '}
                    <span className="fw-500">min</span>
                  </div> */}
                  <div className="mb-1">
                    <span className="fw-500 ml-1 mr-2">Last Attempt</span>
                    <span className="custom-select-option primary">
                      <select disabled={!this.state.delayedCallBackCheckbox} className="form-control d-inline w-auto" onChange={this._setLastAttempt.bind(this)}>
                        <option key={"0"} defaultValue value='Voicemail'>Voicemail</option>
                        <option key={"1"} value='Hangup'>Hangup</option>
                      </select>
                    </span>
                  </div>
                  <button disabled={!this.state.delayedCallBackCheckbox} className="btn bg-lightblue btn-add" type="button" onClick={this._setDynamicAttempt.bind(this)}>
                    <i className="fa fafa fa-plus" />
                  </button>
                </div>
              </div>
            </div>

            <h4>Alerts</h4>
            <div className="cont alerts">
              <div className="wrapper">
                <div className="custom-checkbox">
                  <input type="checkbox" checked={this.state.wordAlertCheckbox} />
                  <label htmlFor="checkbox03" className="mb-1" onClick={this._setWordAlert.bind(this)}>
                    <span className="text-lightblue">Specific word alerts</span>
                  </label>
                </div>
                <textarea
                  className="form-control"
                  name=""
                  rows="6"
                  placeholder="Donec, commodo, nisi, eget, cursus, arcu, augue, integer, luctus, finibus, neque, mattis"
                  disabled={!this.state.wordAlertCheckbox}
                  onChange={this._setWordAlertText.bind(this)}
                />

                <br /><br />
                {this.state.dynamicNotifyEmail.map(function (val, index) {
                  if (index == 0) {
                    return (<div className="ml-4 pl-2 mb-1">
                      <span className="text-lightblue fw-500 mr-2">Notify</span>&nbsp;
                      <input disabled={!this.state.wordAlertCheckbox} type="email" className="form-control form-lightblue d-inline" placeholder="john@gmail.com" value={this.state.notifySpecificWordEmailList[index]} onChange={this._setNotifySpecificWordEmailList.bind(this, index)} />
                    </div>
                    )

                  }
                  else {
                    return (<div className="ml-5 pl-3 mb-1">
                      <span className="text-lightblue fw-500 mr-2">&nbsp;</span>
                      <input disabled={!this.state.wordAlertCheckbox} type="email" className="form-control form-lightblue d-inline" placeholder="john@gmail.com" value={this.state.notifySpecificWordEmailList[index]} onChange={this._setNotifySpecificWordEmailList.bind(this, index)} />
                      {this.state.wordAlertCheckbox ?
                        <span>&nbsp;<i className="fa fa-trash-o" onClick={this._removeNotifyEmail.bind(this, index)} aria-hidden="true"></i>
                        </span> : <span>&nbsp;<i className="fa fa-trash-o" aria-hidden="true"></i></span>}
                    </div>
                    )
                  }

                }, this)}

                {/* <div className="ml-4 pl-2 mb-1">
                  <span className="text-lightblue fw-500 mr-2">Notify</span>
                  <input type="email" className="form-control form-lightblue d-inline" value="john@email.com" />
                </div>
                <div className="ml-5 pl-3 mb-1">
                  <span className="text-lightblue fw-500 mr-2">&nbsp;</span>
                  <input type="email" className="form-control form-lightblue d-inline" value="john@email.com" />
                </div> */}
                <div className="ml-5 pl-4">
                  &nbsp;<button disabled={!this.state.wordAlertCheckbox} className="btn bg-lightblue btn-add" type="button" onClick={this._createNotifyEmail.bind(this)}>
                    <i className="fa fa-plus" />
                  </button>
                </div>
                <br /><br />
                <div className="custom-checkbox low-score-alert">
                  <input type="checkbox" id="checkbox04" checked={this.state.wordAlertCheckboxThreshold} />
                  <label htmlFor="checkbox04" className="mb-1" onClick={this._setWordAlertThreshold.bind(this)}>
                    <span className="text-lightblue">Low Score Alert Threshold</span>


                  </label>
                  &nbsp; &nbsp; &nbsp; &nbsp;
                  <input disabled={!this.state.wordAlertCheckboxThreshold} type="number" className="form-control form-lightblue d-inline" onChange={this._setThresholdNumber.bind(this)} style={numStyle} value={this.state.threshold} />
                  &nbsp; &nbsp; &nbsp; &nbsp;
                </div>
                <br />
                {this.state.dynamicNotifyEmailThreshold.map(function (val, index) {
                  if (index == 0) {
                    return (<div className="ml-4 pl-2 mb-1">
                      <span className="text-lightblue fw-500 mr-2">Notify</span>&nbsp;
                      <input disabled={!this.state.wordAlertCheckboxThreshold} type="email" className="form-control form-lightblue d-inline" placeholder="john@gmail.com" value={this.state.notifyThresholdWordEmailList[index]} onChange={this._setNotifyThresholdWordEmailList.bind(this, index)} />
                    </div>
                    )

                  }
                  else {
                    return (<div className="ml-5 pl-3 mb-1">
                      <span className="text-lightblue fw-500 mr-2">&nbsp;</span>
                      <input disabled={!this.state.wordAlertCheckboxThreshold} type="email" className="form-control form-lightblue d-inline" placeholder="john@gmail.com" value={this.state.notifyThresholdWordEmailList[index]} onChange={this._setNotifyThresholdWordEmailList.bind(this, index)} />
                      {this.state.wordAlertCheckboxThreshold ?
                        <span>&nbsp;<i className="fa fa-trash-o" onClick={this._removeNotifyEmailThreshold.bind(this, index)} aria-hidden="true"></i></span>
                        : <span>&nbsp;<i className="fa fa-trash-o" aria-hidden="true"></i></span>
                      }
                    </div>
                    )
                  }

                }, this)}
                {/* <div className="ml-4 pl-2 mb-1">
                  <span className="text-lightblue fw-500 mr-2">Notify</span>
                  <input type="email" className="form-control form-lightblue d-inline" value="john@email.com" />
                </div>
                <div className="ml-5 pl-3 mb-1">
                  <span className="text-lightblue fw-500 mr-2">&nbsp;</span>
                  <input type="email" className="form-control form-lightblue d-inline" value="john@email.com" />
                </div> */}
                <div className="ml-5 pl-4">
                  &nbsp;<button disabled={!this.state.wordAlertCheckboxThreshold} className="btn bg-lightblue btn-add" type="button" onClick={this._createNotifyEmailThreshold.bind(this)}>
                    <i className="fa fa-plus" />
                  </button>
                </div>
              </div>
            </div>

            <button onClick = {this._setJsonStruc.bind(this)} className="btn bg-lightblue btn-create-campaign" type="button">
              <i className="fa fa-plus-circle" />
              <span>CREATE CAMPAIGN</span>
            </button>
          </form>
        </div>
      </section>
    );
  }
}
function mapStateToProps(state) {
  return {
    audienceList:state.dashboardReducer.audienceList.data,
    user:state.auth.user.name,
    surveyList:state.dashboardReducer.surveyList.data
    
  };
}

export default connect(mapStateToProps, { getAudienceList, getsurveyDashboardList,getCurrentUser })(CreateCampaign);
