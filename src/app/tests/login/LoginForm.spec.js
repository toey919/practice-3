import React from 'react';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import LoginForm from '../../components/login/LoginForm';

configure({ adapter: new Adapter() });

test('Login Form being tested', () => {
  // Render Login Form
  const loginForm = shallow(<LoginForm/>);
  expect(loginForm.contains(
    <h1>Login</h1>
)).toBe(true)
});