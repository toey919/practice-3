$(document).ready(function() {
	$(".navicon").click(function() {
		$(".sidebar, .right-section, .navicon").toggleClass("active");
	});

	$(".right-section.active, .custom-select-option").click(function() {
		$(this).toggleClass("active");
	});


	// Datepicker
	var dateToday = new Date(); 
	$( function() {
	  var dateFormat = "mm/dd/yy",
	    from = $( "#from" )
	      .datepicker({
	        defaultDate: dateToday,
	        changeMonth: true,
	        changeYear: true,
		    dateFormat: 'dd/mm/y', 
	        numberOfMonths: 2,
	        minDate: dateToday
	      })
	      .on( "change", function() {
	        to.datepicker( "option", "minDate", getDate( this ) );
	      }),
	    to = $( "#to" ).datepicker({
	      defaultDate: "+1w",
		    changeMonth: true,
		    changeYear: true,
		    dateFormat: 'dd/mm/y', 
	      numberOfMonths: 2
	    })
	    .on( "change", function() {
	      from.datepicker( "option", "maxDate", getDate( this ) );
	    });

	  function getDate( element ) {
	    var date;
	    try {
	      date = $.datepicker.parseDate( dateFormat, element.value );
	    } catch( error ) {
	      date = null;
	    }

	    return date;
	  }
	} );

	$( function() {
	  $( ".datepicker" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  dateFormat: 'dd/mm/y' 
	  });
	});


	// Enable and Disable slider
	$( ".day-1" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range1" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range1" ).slider( "enable" );
	  }
	});

	$( ".day-2" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range2" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range2" ).slider( "enable" );
	  }
	});

	$( ".day-3" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range3" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range3" ).slider( "enable" );
	  }
	});

	$( ".day-4" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range4" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range4" ).slider( "enable" );
	  }
	});

	$( ".day-5" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range5" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range5" ).slider( "enable" );
	  }
	});

	$( ".day-6" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range6" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range6" ).slider( "enable" );
	  }
	});

	$( ".day-7" ).on( "click", function() {
	  $(this).toggleClass("active");
	  $( ".slider-range7" ).slider( "disable" );

	  if ( $(this).hasClass("active") ) {
	  	$( ".slider-range7" ).slider( "enable" );
	  }
	});

	// If checkbox is true slider enabled else disabled
	$( "#check-slider-1" ).on( "click", function() {
	  if (this.checked == true){
	    $( ".slider-range8" ).slider( "enable" );
	  } else {
	    $( ".slider-range8" ).slider( "disable" );
	  }
	});

	$( "#check-slider-2" ).on( "click", function() {
	  if (this.checked == true){
	    $( ".slider-range9" ).slider( "enable" );
	  } else {
	    $( ".slider-range9" ).slider( "disable" );
	  }
	});

	// Slider 
	$(".slider-range").slider({
		animate: "fast",
		disabled: true,
	  range: true,
	  min: 0,
	  max: 1440,
	  step: 30,
	  values: [540, 1020]
	});	

	// Slider 1
	$(".slider-range1").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	

	  	$('.start-time1').val(hours1 + ':' + minutes1);
	    $('.end-time1').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider2
	$(".slider-range2").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time2').val(hours1 + ':' + minutes1);
	    $('.end-time2').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider3
	$(".slider-range3").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time3').val(hours1 + ':' + minutes1);
	    $('.end-time3').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider4
	$(".slider-range4").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time4').val(hours1 + ':' + minutes1);
	    $('.end-time4').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider5
	$(".slider-range5").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time5').val(hours1 + ':' + minutes1);
	    $('.end-time5').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider6
	$(".slider-range6").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time6').val(hours1 + ':' + minutes1);
	    $('.end-time6').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider7
	$(".slider-range7").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time7').val(hours1 + ':' + minutes1);
	    $('.end-time7').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider8
	$(".slider-range8").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time8').val(hours1 + ':' + minutes1);
	    $('.end-time8').val(hours2 + ':' + minutes2);
	  }
	});

	// Slider9
	$(".slider-range9").slider({
	  slide: function (event, ui) {
			// Start time
			var hours1 = Math.floor(ui.values[0] / 60);
			var minutes1 = ui.values[0] - (hours1 * 60);

			if (hours1.length == 1) hours1 = '0' + hours1;
			if (minutes1.length == 1) minutes1 = '0' + minutes1;
			if (minutes1 == 0) minutes1 = '00';
			if (hours1 >= 12) {
			  if (hours1 == 12) {
			      hours1 = hours1;
			      minutes1 = minutes1 + "PM";
			  } else {
			      hours1 = hours1 - 12;
			      minutes1 = minutes1 + "PM";
			  }
			} else {
			    hours1 = hours1;
			    minutes1 = minutes1 + "AM";
			}
			if (hours1 == 0) {
			    hours1 = 12;
			    minutes1 = minutes1;
			}
			if (hours1 < 10) {
				hours1 = "0" + hours1;
			}

			// End time
			var hours2 = Math.floor(ui.values[1] / 60);
			var minutes2 = ui.values[1] - (hours2 * 60);

			if (hours2.length == 1) hours2 = '0' + hours2;
			if (minutes2.length == 1) minutes2 = '0' + minutes2;
			if (minutes2 == 0) minutes2 = '00';
			if (hours2 >= 12) {
			    if (hours2 == 12) {
			        hours2 = hours2;
			        minutes2 = minutes2 + "PM";
			    } else if (hours2 == 24) {
			        hours2 = 11;
			        minutes2 = "59PM";
			    } else {
			        hours2 = hours2 - 12;
			        minutes2 = minutes2 + "PM";
			    }
			} else {
			    hours2 = hours2;
			    minutes2 = minutes2 + "AM";
			}
			if (hours2 < 10) {
				hours2 = "0" + hours2;
			}	
				
	  	$('.start-time9').val(hours1 + ':' + minutes1);
	    $('.end-time9').val(hours2 + ':' + minutes2);
	  }
	});

	// If active it will enabled the slider else disabled
	if ($(".day-1").hasClass("active")) {
		$(".slider-range1").slider("enable");
	}
	else {
		$(".slider-range1").slider("disable");
	}

	if ($(".day-2").hasClass("active")) {
		$(".slider-range2").slider("enable");
	}
	else {
		$(".slider-range2").slider("disable");
	}

	if ($(".day-3").hasClass("active")) {
		$(".slider-range3").slider("enable");
	}
	else {
		$(".slider-range3").slider("disable");
	}

	if ($(".day-4").hasClass("active")) {
		$(".slider-range4").slider("enable");
	}
	else {
		$(".slider-range4").slider("disable");
	}

	if ($(".day-5").hasClass("active")) {
		$(".slider-range5").slider("enable");
	}
	else {
		$(".slider-range5").slider("disable");
	}

	if ($(".day-6").hasClass("active")) {
		$(".slider-range6").slider("enable");
	}
	else {
		$(".slider-range6").slider("disable");
	}

	if ($(".day-7").hasClass("active")) {
		$(".slider-range7").slider("enable");
	}
	else {
		$(".slider-range7").slider("disable");
	}

	// If checked it will enabled the slider else disabled
	if ($("#check-slider-1").attr("checked")) {
	  $( ".slider-range8" ).slider( "enable" );
	} else {
	  $(".slider-range8").slider("disable");
	}

	if ($("#check-slider-2").attr("checked")) {
	  $( ".slider-range9" ).slider( "enable" );
	} else {
	  $(".slider-range9").slider("disable");
	}

	// $( "#add-audience" ).on( "click", function() {
	// 	$( "#cont-audience" ).append('<div class="custom-select-option mt-4"><select class="form-control"><option class="d-none" selected>Select audience</option><option>Audience Group 1</option><option>Audience Group 2</option><option>None</option></select></div>');
 //  });

	$('ul.campaign-tabs').each(function(){
	  // For each set of tabs, we want to keep track of
	  // which tab is active and its associated content
	  var $active, $content, $links = $(this).find('a');

	  // If the location.hash matches one of the links, use that as the active tab.
	  // If no match is found, use the first link as the initial active tab.
	  $active = $($links.filter('[href="'+location.hash+'"]')[1] || $links[0]);
	  $active.addClass('active');

	  $content = $($active[0].hash);

	  // Hide the remaining content
	  $links.not($active).each(function () {
	    $(this.hash).hide();
	  });

	  // Bind the click event handler
	  $(this).on('click', 'a', function(e){
	    // Make the old tab inactive.
	    $active.removeClass('active');
	    $content.hide();

	    // Update the variables with the new link and content
	    $active = $(this);
	    $content = $(this.hash);

	    // Make the tab active.
	    $active.addClass('active');
	    $content.show();

	    // Prevent the anchor's default click action
	    e.preventDefault();
	  });
	});

  // Textarea default value
	$(function() {
	    $('textarea').val();
	});

	// textarea will automatically expand or resize 
	jQuery.each(jQuery('textarea[data-autoresize]'), function() {
	    var offset = this.offsetHeight - this.clientHeight;
	 
	    var resizeTextarea = function(el) {
	        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
	    };
	    jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
	});

	// Toggle button new audience
	$( "#btn-new-audience" ).on( "click", function() {
	  $(this).addClass("d-none");
	  $("#input-new-audience" ).removeClass( "d-none" );
	  $("#search-audience" ).addClass( "d-none" );
	  $("#heading-audience").text("New Audience");
	});
});

